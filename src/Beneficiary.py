import datetime
import json
import logging
from Cowin import Cowin


class Beneficiary:

    SOURCE_API = "API"
    SOURCE_CONFIG = "CONFIG"

    def __init__(self, beneficiary, source=SOURCE_API):
        self.logger = logging.getLogger(__name__)

        if (source == self.SOURCE_API):
            self.load_from_api(beneficiary)
        elif (source == self.SOURCE_CONFIG):
            self.load_from_dict(beneficiary)

    def load_from_api(self, beneficiary):
        self.bref_id = beneficiary["beneficiary_reference_id"]
        self.name = beneficiary["name"]
        self.vaccine = beneficiary["vaccine"]
        self.appointments = beneficiary["appointments"]
        self.age = datetime.datetime.today().year - int(
            beneficiary["birth_year"]
        )
        self.status = beneficiary["vaccination_status"]
        self.dose1_date = beneficiary["dose1_date"]

        self.set_dose_num()
        self.set_dose2_due_date()

    def load_from_dict(self, beneficiary):
        self.bref_id = beneficiary["bref_id"]
        self.name = beneficiary["name"]
        self.vaccine = beneficiary["vaccine"]
        self.appointments = beneficiary["appointments"]
        self.age = beneficiary["age"]
        self.status = beneficiary["status"]
        self.dose1_date = beneficiary["dose1_date"]

        self.set_dose_num()
        self.set_dose2_due_date()

    def is_vaccine_needed(self):
        if (self.status not in [Cowin.VACCINATED_NO, Cowin.VACCINATED_PARTIAL]):
            # Already vaccinated
            return False
        elif (self.status == Cowin.VACCINATED_PARTIAL and len(self.appointments) > 1):
            # Vaccinated and appointment booked
            return False
        elif(self.status == Cowin.VACCINATED_PARTIAL and (datetime.datetime.strptime(self.dose2_due_date, "%d-%m-%Y")-datetime.datetime.today()).days > 7):
            # Vaccine 2 due date not within 7 days
            return False

        # TODO: Fill other conditions where vacciene need not be booked

        return True

    def set_dose_num(self):
        self.dose_num = 1
        if (self.status == Cowin.VACCINATED_PARTIAL):
            self.dose_num = 2

    def set_dose2_due_date(self):
        self.dose2_due_date = ""

        if (self.status == Cowin.VACCINATED_PARTIAL):
            wait_period = Cowin.vaccine_dose2_duedate(self.vaccine)
            dose1_date = datetime.datetime.strptime(
                self.dose1_date, "%d-%m-%Y")

            self.dose2_due_date = dose1_date + \
                datetime.timedelta(days=wait_period)

    def __str__(self):
        st = "\n[beneficiary]\n"
        st += f"bref_id={json.dumps(self.bref_id)}\n"
        st += f"name={json.dumps(self.name)}\n"
        st += f"vaccine={json.dumps(self.vaccine)}\n"
        st += f"appointments={json.dumps(self.appointments)}\n"
        st += f"age={json.dumps(self.age)}\n"
        st += f"status={json.dumps(self.status)}\n"
        st += f"dose1_date={json.dumps(self.dose1_date)}\n"

        return st

    def get_config_dict(self):
        return {
            "bref_id": self.bref_id,
            "name": self.name,
            "vaccine": self.vaccine,
            "appointments": self.appointments,
            "age": self.age,
            "status": self.status,
            "dose1_date": self.dose1_date,
        }

from Database import Database
from Cowin import Cowin
from Cobook import Cobook
from Beneficiary import Beneficiary
import os
import time
import datetime
import jwt
import logging
import random
import argparse
import toml

logger = logging.getLogger("main")


parser = argparse.ArgumentParser(
    description='Book Covid-19 vaccine from the cowin.gov.in portal for India')
parser.add_argument('-a', '--auto', action='store_true',
                    help='Switch to automatic entry of OTP and refresh interval')
parser.add_argument('-c', '--from-config', action='store_true',
                    help='Run using the config.toml file without any prompt')
options = parser.parse_args()


def main():
    logging.basicConfig(
        level=logging.INFO,
        format="[%(name)-11s] %(asctime)s | %(levelname)-5s | %(message)s",
        datefmt="%Y/%m/%d - %H:%M:%S",
        # filename=('debug.log')
    )

    logger.info("Application started")
    cobook = Cobook()
    cowin = Cowin()

    db = Database()
    db.migrations()
    db.con.close()

    # Check or ask for config
    beneficiaries = ""
    load_from_config = False

    if (os.path.isfile(cobook.CONFIG_PATH) and not options.from_config):
        logger.info("Previous configuration found")
        ch = input("\nWant to reuse previous configuration? (y/n): ")

        if (ch == "y"):
            load_from_config = True

    if (load_from_config or options.from_config):
        beneficiaries = cobook.load_from_config()

    else:
        phone = cobook.choose_phone()

        get_previous_token(phone, cowin)

        # Fetch and choose a beneficiary
        beneficiaries_all = [Beneficiary(b) for b in cowin.get_beneficiaries()]
        beneficiaries = cobook.choose_beneficiaries(beneficiaries_all)

        if (not check_if_valid_beneficiaries(beneficiaries)):
            logger.error("Beneficiaries should be of the same type")

        cobook.phone = phone

        # Choose / set the vaccine preference
        if (beneficiaries[0].status != Cowin.VACCINATED_PARTIAL):
            cobook.vaccine = cobook.choose_vaccine_preference()

        # Choose the location preference
        cobook.location_type = cobook.choose_location_type_preference()

        if (cobook.location_type == cobook.LOCATION_BY_DISTRICT):
            cobook.location_code = cobook.choose_district(cowin)
        else:
            cobook.location_code = cobook.choose_pincode()

        # Choose refresh interval
        cobook.refresh_interval = cobook.choose_refresh_interval()

        # Choose minimum availability
        cobook.availability = cobook.choose_min_availability(
            len(beneficiaries))

        # Choose start date
        max_dose2_due = beneficiaries[0].dose2_due_date
        for b in beneficiaries:
            if b.dose2_due_date > max_dose2_due:
                max_dose2_due = b.dose2_due_date

        if (beneficiaries[0].status == Cowin.VACCINATED_PARTIAL and (datetime.datetime.strptime(max_dose2_due, "%d-%m-%Y") - datetime.datetime.today()).days < 0):
            print("Starting vaccine search from %s" % (
                datetime.datetime.strptime(max_dose2_due, "%d-%m-%Y")))

            cobook.start_date = (datetime.datetime.strptime(
                max_dose2_due, "%d-%m-%Y")-datetime.datetime.today()).days + 2
        else:
            cobook.start_date = cobook.choose_start_date()

        # Choose fee preference
        cobook.fee_preference = cobook.choose_fee_type_preference()

        # Choose fee limit
        cobook.fee_limit = cobook.choose_fee_limit(
            cobook.fee_preference)

        # Choose capctcha automation
        # TODO: Maybe set as optional? (Check this)
        cobook.captcha_automated = cobook.choose_captcha_automated()

    if (not options.from_config):
        print("\nYour are going to continue with the following configuration\n")
        print(toml.dumps(cobook.get_full_config(beneficiaries)).strip())
        ch = input("\nSure to continue? (y/n): ")

        if (ch != "y"):
            return 0

    if (cowin.token == None):
        get_previous_token(cobook.phone, cowin)

    # Save the configuration for future use
    cobook.save_configuration(beneficiaries)

    booked = False
    # From a comment from https://github.com/cowinapi/developer.cowin/issues/382#issuecomment-854044099
    while (not booked):

        try:
            # Calculate the actual date
            start_date = (datetime.datetime.today(
            ) + datetime.timedelta(days=cobook.start_date - 1)).strftime("%d-%m-%Y")

            vaccination_centers_all = []
            if (cobook.location_type == Cobook.LOCATION_BY_DISTRICT):
                vaccination_centers_all = cowin.get_vaccination_by_districts(
                    start_date, cobook.location_code, cobook.vaccine)
            else:
                vaccination_centers_all = cowin.get_vaccination_by_pincode(
                    start_date, cobook.location_code, cobook.vaccine)

            # from test_avail import centers
            # vaccination_centers = centers
            cowin.token_limit -= 1
            db = Database()
            db.save_token(cobook.phone, cowin.token, cowin.token_limit)
            db.con.close()

            vaccination_centers = filter_centers_for_beneficiaries(
                vaccination_centers_all, beneficiaries, cobook)

            if (len(vaccination_centers) > 0):
                logger.info("Found %d centers with vaccine available" %
                            (len(vaccination_centers)))

                beneficiary_ids = [b.bref_id for b in beneficiaries]

                # Try 1-3 times in random centers / slots before retrying
                for _ in range(min(3, len(vaccination_centers))):
                    center = shuffle_and_choose_sessions(vaccination_centers)
                    random_slot = center["slots"][random.randrange(
                        0, len(center["slots"]))]

                    logger.info(
                        f"Trying to book in {center['name']}, {center['availability']} {center['vaccine']} available, {random_slot} slot")
                    try:
                        appointment_id = cowin.book(
                            beneficiaries[0].dose_num, center["session_id"], random_slot, beneficiary_ids)
                        booked = True

                        logger.info(f"Appointment id is {appointment_id}")
                        cobook.clear_configuration()

                        logger.info(f"Saving appointment slip")
                        cowin.save_appointement(
                            appointment_id, cobook.phone, beneficiaries[0].dose_num)

                    except Exception as e:
                        if (str(e) == Cowin.ERROR_CENTER_BOOKED_FULL):
                            # Try again
                            pass
                        elif (str(e) == Cowin.ERROR_BOOKING_UNKNOWN):
                            # Retry breaking the loop
                            break
                        else:
                            print(e)
                            break

                    if (booked):
                        break

        except Exception as e:
            if (str(e) == cowin.ERROR_TOKEN_EXPIRED):
                logger.info("Renewing token")
                authenticate(cowin, cobook.phone)

            elif (str(e) == cowin.ERROR_RATE_LIMITED):
                cowin.handle_rate_limited()
            else:
                logger.error(f"Exception: {e}")

        logger.info(
            f"Current token has {remaining_token_time(cowin.token)}s and {cowin.token_limit} calls remaining")
        if (not options.auto):
            logger.info(
                f"Fetching next list after pressing \"ENTER\" for {start_date} in {len(vaccination_centers_all)} centers")
            input()
        else:
            random_interval = random.randint(
                cobook.refresh_interval[0], cobook.refresh_interval[1])

            logger.info(
                f"Fetching next list in {random_interval}s for {start_date} in {len(vaccination_centers_all)} centers")
            time.sleep(random_interval)

    logger.info("Application ended")
    return 0


def check_if_valid_beneficiaries(beneficiaries):
    all_dose_no = []
    vaccine_type = []
    for b in beneficiaries:
        all_dose_no.append(b.dose_num)
        vaccine_type.append(b.vaccine)

    if (len(list(set(all_dose_no))) > 1):
        logger.error("All benificiaries should have the same dose number")
        return False
    elif (len(list(set(vaccine_type))) > 1):
        logger.error("All benificiaries should have the same vaccine type")
        return False

    return True


# Load / generate token. Fetch token from database
def get_previous_token(phone, cowin):
    # check if it works else get new token
    try:
        db = Database()
        token = db.get_token(phone)
        db.con.close()

        if (token != None and is_token_valid(token["token"])):
            cowin.token = token["token"]
            cowin.token_limit = token["limit"]
            logger.info("Using token from previous session")
        else:
            raise Exception(cowin.ERROR_TOKEN_EXPIRED)

    except Exception as e:
        if (
            str(e) == cowin.ERROR_TOKEN_EXPIRED or
            str(e) == db.ERROR_TOKEN_NO
        ):
            logger.info("Trying to get new token")
            authenticate(cowin, phone)
        else:
            raise e


def shuffle_and_choose_sessions(centers):
    result = sorted(
        centers, key=lambda center: center["availability"], reverse=True)
    total_count = sum([center["availability"] for center in centers])

    # Weighted random number selection. So the session with more number of
    # availability gets more change of being selected
    count_series = int(random.random() * total_count) + 1
    for center in centers:
        if (center["availability"] >= count_series):
            return center

        count_series -= center["availability"]

    return result


def filter_centers_for_beneficiaries(centers, beneficiaries, cobook):

    # Payment preference filter
    filtered_centers = list(filter(lambda center: (
        (cobook.fee_preference ==
         Cowin.FEE_NO_PREF or cobook.fee_preference == center["fee_type"])
    ), centers))

    min_age = min([b.age for b in beneficiaries])

    # All other filtering
    result = []
    for center in filtered_centers:
        vaccine_fees = []
        try:
            vaccine_fees = center["vaccine_fees"]
        except KeyError:
            pass

        for session in center["sessions"]:
            if (
                session[f'available_capacity_dose{beneficiaries[0].dose_num}'] >= cobook.availability and
                session["min_age_limit"] <= min_age and
                (cobook.vaccine == Cowin.VACCINE_NONE or cobook.vaccine == session["vaccine"]) and
                is_price_below(
                    vaccine_fees, session["vaccine"], cobook.fee_limit)
            ):
                result.append({
                    "session_id": session["session_id"],
                    "name": center["name"],
                    "state": center["state_name"],
                    "district": center["district_name"],
                    "block": center["block_name"],
                    "pincode": center["pincode"],
                    "center_id": center["center_id"],
                    "vaccine": session["vaccine"],
                    "fee_type": center["fee_type"],
                    "availability": session[f'available_capacity_dose{beneficiaries[0].dose_num}'],
                    "date": session["date"],
                    "slots": session["slots"],
                })

    return result


# Check if the price of the currest vacccine is below the
# user specified threshold
def is_price_below(vaccine_fees, vaccine, fee_limit):
    # Check if current vaccine exists in price list
    for v in vaccine_fees:
        if (v["vaccine"] == vaccine and int(v["fee"]) > fee_limit):
            return False

    return True


# Send OTP and complete authentication to get token
# a new token
def authenticate(cowin, phone):
    try:
        cowin.send_otp(phone)
    except Exception as e:
        logger.error(e)
        raise Exception("Couldn't request OTP")

    otp = None
    if (not options.auto):
        otp = input("\nEnter your OTP: ")
    else:
        logger.info("Waiting for OTP update in database")
        db = Database()
        counter = 0
        while otp == None:
            db = Database()
            otp = db.get_otp(phone)
            time.sleep(5)  # Wait till next check of OTP from DB
            counter += 5
            if (counter > 60 * 5):
                raise Exception(
                    "OTP was not updated in DB even after 5 minutes")
        db.con.close()

    token = cowin.validate_otp(otp)

    # Save token in DB (maybe for a futher restart)
    db = Database()
    db.save_token(phone, token, Database.TOKEN_DEFAULT_LIMIT)
    db.con.close()

    return token


def remaining_token_time(token):
    payload = jwt.decode(token, options={"verify_signature": False})
    remaining_seconds = payload['exp'] - int(time.time())

    return remaining_seconds


def is_token_valid(token):
    remaining_seconds = 0
    try:
        remaining_seconds = remaining_token_time(token)
    except jwt.ExpiredSignatureError:
        return False

    if remaining_seconds <= 1*30:  # 30 secs early before expiry for clock issues
        return False
    if remaining_seconds <= 60:
        logger.debug("Token is about to expire in next 1 min...")
    return True


if __name__ == '__main__':
    main()

from flask import Flask, request, jsonify
import json as js
from Database import Database
import re

app = Flask(__name__)
db = Database()
db.migrations()


def json(data):
    return js.dumps(data)


@app.route("/<phone>/otp", methods=['PUT'])
def add_otp(phone):
    db = Database()
    if (request.json == None or "message" not in request.json):
        return json({"message": "No OTP given"}), 400

    otp = re.findall(r'[0-9]{6}', request.json["message"])
    if (len(otp) != 1):
        return json({"message": "Couldn't find OTP from message"}), 400
    otp = otp[0]

    db.save_otp(phone, otp)
    return ""


# @app.route("/<phone>/otp", methods=['GET'])
# def get_otp(phone):
#     db = Database()
#     otp = None
#     try:
#         otp = db.get_otp(phone)
#     except Exception as e:
#         print("Error", e)
#         return "", 404

#     return jsonify({"otp": otp})


# @app.route("/otp", methods=['GET'])
# def get_all_otp():
#     db = Database()
#     return jsonify(db.get_all_otp())


if __name__ == '__main__':
    app.run("0.0.0.0", port="5000")

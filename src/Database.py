import sqlite3


class Database:
    TOKEN_DEFAULT_LIMIT = 20

    def __init__(self):
        self.con = sqlite3.connect('app.db', check_same_thread=False)
        self.ERROR_TOKEN_NO = "No token for this phone"

    def migrations(self):
        # Create the tables
        cur = self.con.cursor()
        cur.execute('''CREATE TABLE IF NOT EXISTS otps (
            phone VARCHAR(15) NOT NULL PRIMARY KEY,
            otp VARCHAR(15) NULL)''')
        cur.execute(f"""CREATE TABLE IF NOT EXISTS tokens (
            phone VARCHAR(15) NOT NULL PRIMARY KEY,
            token VARCHAR(15) NULL,
            max_limit INTEGER DEFAULT {self.TOKEN_DEFAULT_LIMIT})""")
        self.con.commit()

        return

    def save_token(self, phone, token, limit=TOKEN_DEFAULT_LIMIT):
        try:
            self.get_token(phone)

            # Update token if exists
            cur = self.con.cursor()
            cur.execute("UPDATE tokens SET token=?, max_limit=? WHERE phone=?",
                        (token, limit, phone))
            self.con.commit()

        except Exception:
            # Else insert OTP
            cur = self.con.cursor()
            cur.execute(
                "INSERT INTO tokens (phone, token, max_limit) VALUES (?, ?, ?)", (phone, token, self.TOKEN_DEFAULT_LIMIT))
            self.con.commit()
        return

    def get_token(self, phone):
        cur = self.con.cursor()
        cur.execute(
            "SELECT token, max_limit FROM tokens WHERE phone='%s' LIMIT 1" % (phone))
        data = cur.fetchone()
        if (data == None):
            raise Exception(self.ERROR_TOKEN_NO)
        return {"token": data[0], "limit": data[1]}

    def save_otp(self, phone, otp):
        try:
            self.get_otp(phone)

            # Update OTP if exists
            cur = self.con.cursor()
            cur.execute("UPDATE otps SET otp=? WHERE phone=?",
                        (otp, phone))
            self.con.commit()

        except Exception:
            # Else insert OTP
            cur = self.con.cursor()
            cur.execute(
                "INSERT INTO otps (phone, otp) VALUES (?, ?)", (phone, otp))
            self.con.commit()

        return

    def get_otp(self, phone):
        cur = self.con.cursor()
        cur.execute("SELECT otp FROM otps WHERE phone='%s' LIMIT 1" % (phone))
        data = cur.fetchone()
        if (data == None):
            raise Exception("No OTP for this phone")
        return data[0]

    def get_all_otp(self):
        cur = self.con.cursor()
        cur.execute('SELECT phone, otp FROM otps')
        return cur.fetchall()

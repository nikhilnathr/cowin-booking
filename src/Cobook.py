from Cowin import Cowin
from Beneficiary import Beneficiary
import toml
import os
import logging


class Cobook:
    LOCATION_BY_DISTRICT = "DISTRICT"
    LOCATION_BY_PINCODE = "PINCODE"

    DATE_NO_PREF = "No Preference"

    CONFIG_PATH = os.path.dirname(
        os.path.realpath(__file__)) + "/../config.toml"

    def __init__(self):
        self.logger = logging.getLogger(__name__)

        self.phone = ""
        self.vaccine = Cowin.VACCINE_NONE
        self.location_type = None
        self.location_code = 0
        self.refresh_interval = [20, 60]
        self.availability = 1
        self.start_date = self.DATE_NO_PREF
        self.fee_preference = Cowin.FEE_NO_PREF
        self.fee_limit = 0

    def get_config_dict(self):
        return {
            "phone": self.phone,
            "location_type": self.location_type,
            "location_code": self.location_code,
            "vaccine": self.vaccine,
            "refresh_interval": self.refresh_interval,
            "availability": self.availability,
            "start_date": self.start_date,
            "fee_preference": self.fee_preference,
            "fee_limit": self.fee_limit,
        }

    def set_config_dict(self, booking):
        self.phone = booking["phone"]
        self.vaccine = booking["vaccine"]
        self.location_type = booking["location_type"]
        self.location_code = booking["location_code"]
        self.refresh_interval = booking["refresh_interval"]
        self.availability = booking["availability"]
        self.start_date = booking["start_date"]
        self.fee_preference = booking["fee_preference"]
        self.fee_limit = booking["fee_limit"]

    def load_from_config(self, config_file=CONFIG_PATH):
        config = toml.load(config_file)

        self.set_config_dict(config["booking"])

        return [Beneficiary(beneficiary, source=Beneficiary.SOURCE_CONFIG) for beneficiary in config["beneficiaries"]]

    def get_full_config(self, beneficiaries):
        return {
            "booking": self.get_config_dict(),
            "beneficiaries": [b.get_config_dict() for b in beneficiaries]
        }

    def save_configuration(self, beneficiaries, config_file=CONFIG_PATH):
        self.logger.info("Saving configuration in ./%s" % (config_file))

        config = self.get_full_config(beneficiaries)

        with open(config_file, "w") as f:
            toml.dump(config, f)

    def clear_configuration(self, config_file=CONFIG_PATH):
        self.logger.info("Clearing previous configuration")
        with open(config_file, "w") as f:
            f.write("")

    def show_booked_details(self, dose, session_id, slot, beneficiaries):
        print("")

    def choose_phone(self):
        return input("\nEnter the registered phone number: ")

    def choose_captcha_automated(self):
        return True

    def choose_start_date(self):
        # Get search start date
        start_date = int(input(
            "\nSearch for next seven day starting from when?\nUse 1 for today, 2 for tomorrow, upto 7 for 7th day: "
        ))

        if start_date not in range(1, 8):
            raise Exception("Invalid option")

        return start_date

    def choose_fee_limit(self, fee_preference):
        if (fee_preference == Cowin.FEE_FREE):
            return 0

        return int(input("\nChoose a limit for vaccine price: "))

    def choose_fee_type_preference(self):
        query = "\nHave a fee preference?"
        options = Cowin.AVAILABLE_FEE_METHODS
        default = Cowin.FEE_NO_PREF

        ch = self.choose_option_from_values(
            query, options, default=default, default_string="No Preference")

        if (ch == 0):
            return default
        else:
            return options[ch - 1]

    def choose_min_availability(self, mini):
        query = f"\nFilter out centers with availability less than\n(Minimum {mini}): "
        availability = int(input(query))

        if (availability < mini):
            raise Exception(f"Availability should be atleast {mini}")
        elif (availability > 50):
            self.logger.warn(
                "This is pretty high. Try lowering for better chance.")

        return availability

    def choose_refresh_interval(self):
        query = "\nChoose a refresh interval upper and lower limit to check vaccines in seconds (like 20-50)\n(Minimum 10): "
        interval = sorted(list(map(int, input(query).split("-"))))

        if (min(interval) < 10):
            raise Exception("Interval should be atleast 20")

        return interval

    # Display and choose a beneficiary
    def choose_beneficiaries(self, beneficiaries):
        beneficiaries = [b for b in beneficiaries if b.is_vaccine_needed()]

        if (len(beneficiaries) < 1):
            raise Exception("No beneficiaries in list")

        print("| %-2s | %-30s | %-3s | %-25s | %-15s | %-15s |" %
              ("id", "name", "age", "status", "vaccine", "due"))
        num = 0
        for beneficiary in beneficiaries:
            num += 1
            print("| %2d | %-30s | %3d | %-25s | %-15s | %-15s |" % (num,
                                                                     beneficiary.name, beneficiary.age, beneficiary.status, beneficiary.vaccine, beneficiary.dose2_due_date))

        ch = input(
            '\nEnter the id of beneficiary (if more than one seperate by comma): ')

        return [beneficiaries[int(i)-1] for i in ch.replace(" ", "").split(",")]

    def choose_vaccine_preference(self):
        query = "\nHave a vaccine preference?"
        options = Cowin.AVAILABLE_VACCINES
        default = Cowin.VACCINE_NONE

        ch = self.choose_option_from_values(
            query, options, default=default, default_string="No Preference")

        if (ch == 0):
            return default
        else:
            return options[ch - 1]

    def choose_location_type_preference(self):
        query = "\nHow do you want to select vaccine from?"
        options = [self.LOCATION_BY_DISTRICT, self.LOCATION_BY_PINCODE]

        return options[self.choose_option_from_values(query, options) - 1]

    def choose_district(self, cowin):
        # Select state
        query = "\nSelect a state?"
        states = cowin.get_states()
        options = [state["state_name"] for state in states]

        ch = self.choose_option_from_values(query, options)
        state_id = states[ch-1]["state_id"]

        # Select disctrict
        query = "\nSelect a district?"
        districts = cowin.get_districts(state_id)
        options = [district["district_name"] for district in districts]

        ch = self.choose_option_from_values(query, options)

        return districts[ch-1]["district_id"]

    def choose_pincode(self):
        pincode = input("Enter pincode: ")

        if not pincode or len(pincode) < 6:
            raise Exception("Invalid pincode: %s" % (pincode))

        return int(pincode)

    def choose_option_from_values(self, query, options, default=None, default_string="", showoptions=True):
        opts_string = [str(i+1) + " - " + options[i]
                       for i in range(len(options))]
        opts_string = "\n".join(opts_string)

        full_query = f"{query}\n{opts_string}\n:"
        if (default != None):
            full_query = f"{query}\n0 - {default_string}\n{opts_string}\n(Default 0):"
        if (not showoptions):
            full_query = f"{query}\n0 - {default_string}\n(Default {default_string}):"

        ch = int(input(full_query))

        if (ch not in range(0, len(options) + 1) or (default == None and ch == 0)):
            raise Exception("Invalid choice")
        else:
            return ch

import requests
import time
import datetime
import os
from hashlib import sha256
from Database import Database
import logging


class Cowin:

    base_url = "https://cdn-api.co-vin.in/api"

    VACCINATED_NO = "Not Vaccinated"
    VACCINATED_PARTIAL = "Partially Vaccinated"

    VACCINE_NONE = ""
    VACCINE_COVAXIN = "COVAXIN"
    VACCINE_COVISHIELD = "COVISHIELD"
    VACCINE_SPUTNIK_V = "SPUTNIK V"

    AVAILABLE_VACCINES = [VACCINE_COVAXIN,
                          VACCINE_COVISHIELD, VACCINE_SPUTNIK_V]

    FEE_NO_PREF = ""
    FEE_FREE = "Free"
    FEE_PAID = "Paid"
    AVAILABLE_FEE_METHODS = [FEE_FREE, FEE_PAID]

    ERROR_TOKEN_EXPIRED = "Token expired / incorrect"
    ERROR_RATE_LIMITED = "Rate limited / forbidden"
    ERROR_VACCINATION_CENTERS = "Couldn't retrieve vaccination centers"
    ERROR_CENTER_BOOKED_FULL = "Vaccination center is completely booked for the selected date"
    ERROR_BOOKING_UNKNOWN = "Booking could not be done"

    RATE_LIMIT_SLEEP = 40

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.token = None

    def send_request(self, req, data=None, repetation_counter=0):
        base_request_header = {
            # 'User-Agent': 'Mozilla/5.0 (Linux; Android 9; Redmi Note 5 Build/PQ1A.181105.017.A1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/197.0.0.46.98;]',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
            'origin': 'https://selfregistration.cowin.gov.in/',
            'referer': 'https://selfregistration.cowin.gov.in/'
        }

        if (req["authorized"]):
            if (self.token == None):
                raise Exception("No token for authorized request")

            base_request_header["Authorization"] = f"Bearer {self.token}"

        reqs = {
            "GET": requests.get,
            "POST": requests.post,
        }

        result = reqs[req["type"]](
            url=self.base_url + req["url"], json=data, headers=base_request_header)
        if (result.status_code == 429):
            self.logger.error(
                f"Rate limited. [{result.status_code}] {result.text}")
            raise Exception(self.ERROR_RATE_LIMITED)

        if (result.status_code in [403, 401] and req["authorized"]):
            self.logger.info(
                f"Token expired / incorrect [{result.status_code}] {result.text}")
            raise Exception(self.ERROR_TOKEN_EXPIRED)

        if (result.status_code > 299):
            self.logger.warn(
                f"Some error occured. [{result.status_code}] {result.text}")

        return result

    # Remove previous OTP and request a new OTP
    def send_otp(self, phone):
        req = {
            "type": "POST",
            "url": "/v2/auth/generateMobileOTP",
            "authorized": False,
        }

        # Invalidate previous OTP and token in DB
        db = Database()
        db.save_otp(phone, None)
        db.save_token(phone, None, 0)
        db.con.close()

        # Request new OTP
        self.logger.info("Requesting OTP for mobile number %s" % (phone))
        data = {
            "mobile": phone,
            # TODO: Check why this specific value
            # "secret": "U2FsdGVkX1/ypXZQvWGryYpWQsxFmU6qI2FmuFVH0MHrE9yDuZU2cl2Bx9bPyK6/zAFnVxMyauEiHDhBY12/Tw==",
            "secret": "U2FsdGVkX1/ORMKxsTy42ZqdD2o86KDcH9524cw+0AIzZJKkZjhsUr0i3FnGFNF8rF0eCjcd0fn+/VS7Qr6vfw==",
        }
        txnId = self.send_request(req, data)

        if txnId.status_code != 200:
            self.logger.warn("Unable to send OTP")
            raise Exception("Couldn't send otp")

        txnId = txnId.json()["txnId"]
        self.logger.info("Recieved txnId for OTP '%s'" % (txnId))
        self.txnId = txnId
        return txnId

    # Validated the OTP and sets token
    def validate_otp(self, otp):
        req = {
            "type": "POST",
            "url": "/v2/auth/validateMobileOtp",
            "authorized": False,
        }

        self.logger.info("Validating OTP")
        txnId = ""
        try:
            txnId = self.txnId
        except Exception:
            self.logger.error("No txnId found")
            raise Exception("OTP was not requested (No txnId found)")

        data = {
            "otp": sha256(str(otp.strip()).encode("utf-8")).hexdigest(),
            "txnId": txnId
        }
        token = self.send_request(req, data)

        if token.status_code != 200:
            self.logger.error("Unable to Validate OTP")
            raise Exception("Unable to validate OTP")

        token = token.json()["token"]
        self.logger.info("Token Generated")
        self.token = token
        self.token_limit = Database.TOKEN_DEFAULT_LIMIT

        return token

    def get_beneficiaries(self):
        req = {
            "type": "GET",
            "url": "/v2/appointment/beneficiaries",
            "authorized": True
        }
        beneficiaries = self.send_request(req)

        if beneficiaries.status_code != 200:
            self.logger.error(
                f"Unable to get beneficiaries: {beneficiaries.text}")
            raise Exception("Unable to get beneficiaries")

        return beneficiaries.json()["beneficiaries"]

    def get_states(self):
        req = {
            "type": "GET",
            "url": "/v2/admin/location/states",
            "authorized": False
        }

        result = self.send_request(req)

        if (result.status_code != 200):
            raise Exception("Couldn't retrieve states")

        return result.json()["states"]

    def get_districts(self, state_id):
        req = {
            "type": "GET",
            "url": f"/v2/admin/location/districts/{state_id}",
            "authorized": False
        }
        result = self.send_request(req)

        if (result.status_code != 200):
            raise Exception("Couldn't retrieve states")

        return result.json()["districts"]

    def get_vaccination_by_districts(self, date, code, vaccine):
        req = {
            "type": "GET",
            "url": f"/v2/appointment/sessions/calendarByDistrict?district_id={code}&date={date}",
            "authorized": True
        }

        if (vaccine != Cowin.VACCINE_NONE):
            req["url"] += f"&vaccine={vaccine}"

        result = self.send_request(req)

        if (result.status_code != 200):
            raise Exception(self.ERROR_VACCINATION_CENTERS)

        return result.json()["centers"]

    def get_vaccination_by_pincode(self, date, code, vaccine):
        req = {
            "type": "GET",
            "url": f"/v2/appointment/sessions/calendarByPin?pincode={code}&date={date}",
            "authorized": True
        }

        if (vaccine != Cowin.VACCINE_NONE):
            req["url"] += f"&vaccine={vaccine}"

        result = self.send_request(req)

        if (result.status_code != 200):
            raise Exception(self.ERROR_VACCINATION_CENTERS)

        return result.json()["centers"]

    def book(self, dose, session_id, slot, beneficiaries):
        req = {
            "type": "POST",
            "url": f"/v2/appointment/schedule",
            "authorized": True
        }

        data = {
            "dose": dose,
            "session_id": session_id,
            "slot": slot,
            "beneficiaries": beneficiaries,
        }

        result = self.send_request(req, data)

        if result.status_code == 200:
            self.logger.info(f"Vaccine booked successfully")
            # return result.json()["appointment_id"]
            return result.json()["appointment_confirmation_no"]
        elif result.status_code == 409:
            try:
                data = result.json()
                # Response: 409 : {"errorCode":"APPOIN0040","error":"This vaccination center is completely booked for the selected date. Please try another date or vaccination center."}
                if data.get("errorCode", '') == 'APPOIN0040':
                    self.logger.warn(self.ERROR_CENTER_BOOKED_FULL)
                    raise Exception(self.ERROR_CENTER_BOOKED_FULL)

            except Exception:
                pass

        raise Exception(self.ERROR_BOOKING_UNKNOWN)

    def save_appointement(self, appointment_id, phone, dose_num):
        curdir = os.path.dirname(os.path.realpath(__file__))
        basepath = curdir + "/../docs/"
        filename = f"{phone}-appointment-dose-{dose_num}.pdf"
        fullpath = basepath + filename

        if (not os.path.isdir(basepath)):
            os.mkdir(basepath, 755)

        url = self.base_url + \
            f"/v2/appointment/appointmentslip/download?appointment_id={appointment_id}"
        r = requests.get(url, stream=True)

        with open(fullpath, 'wb') as f:
            f.write(r.content)

        self.logger.info(f"Appointment saved in {fullpath}")

    @staticmethod
    def vaccine_dose2_duedate(vaccine_type):
        """
        This function
            1.Checks the vaccine type
            2.Returns the appropriate due date for the vaccine type
        """
        covishield_due_date = 84
        covaxin_due_date = 28
        sputnikV_due_date = 21

        if vaccine_type == "COVISHIELD":
            return covishield_due_date
        elif vaccine_type == "COVAXIN":
            return covaxin_due_date
        elif vaccine_type == "SPUTNIK V":
            return sputnikV_due_date

    # Handle rate limits
    def handle_rate_limited(self):
        self.logger.warn(
            f"Rate-limited by CoWIN. Waiting for {self.RATE_LIMIT_SLEEP} seconds")
        self.logger.info(
            "You can reduce your refresh frequency. Other devices using CoWIN also contribute the limit")
        time.sleep(self.RATE_LIMIT_SLEEP)

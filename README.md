
# Cobook - Covid Vaccine Booking 

**The new [terms](https://www.cowin.gov.in/terms-condition) for the Cowin site has deemed the use of bots / automated scripts for booking invalid. But I am leaving this project as it is as a POC. Do not use this for the actual booking.**

## Disclaimer 

I or this project does not give any guarantee for the working of this script, and usage of this script for booking may lead to blocking of your number. I do not promote the use of automated scripts. This project is just left for educational reference as a proof of concept.

## Introduction

Automate booking Covid-19 vaccines from the [CoWIN](cowin.gov.in) portal. This script requires some technical know-how to set up correctly.

This project was inspired by [covid-vaccine-booking](https://github.com/bombardier-gif/covid-vaccine-booking) over in Github. Not just inspired, I have used some of their code as I have not tested this project for all scenarios. So, reading the code and modifying it was my only option. A huge thanks to them.

**Automatic mode was changed to be optional since that may lead to the blocking of phone number**

The Cowin site has set limitations on how many searches you can perform in a specific time. The following limits were identified till now,

- 100 requests per a 5-minute interval (after that a rate-limiting for 10 - 20minutes)
- 1000 requests per day (after which your number may be blocked for 24 hours or more)
- Maximum 50 OTP per day (after which your number may be blocked for 24 hours or more)

Moreover, a single token is valid for 15 minutes or 20 requests, whichever comes first.

## Initial setup

### Requirements

- python 3 (tested on v3.8.5) ([install python](https://www.python.org/downloads/))
- pip (probably included with python)
- [virtualenv](https://virtualenv.pypa.io) (optional, recomended)
- IFTTT app (for automode only)

### Clone the repository

```sh
git clone https://gitlab.com/nikhilnathr/cowin-bot.git
cd cowin-bot
```

### Setup the virtual environment

Install `virtualenv` in your system (if not already installed)
```sh
pip install virtualenv
```

Initialize a new virtual environment

```sh
virtualenv env
```

Activate the virtual environment
```sh
source env/bin/activate
```

> For windows to activate the virtual environment \
> ```venv\Scripts\activate```

### Install the required packages

Use pip to install the required packages,

```sh
pip install -r requirements.txt
```

## Running in manual mode

Manual mode is the default mode. In manual mode, you will have to type the OTP and refresh manually. This will help avoid hitting the usage limits, and after each request, the user can analyze and use avoiding the limits.

To run in manual mode,

```
python src/run.py
```

The usage is relatively intuitive for more instruction goto [usage instructions](#Usage).

## Running automatic mode [CAUTION]

**Use this mode only if you know what you are doing.**

This mode needs some more setup as you have to set up the IFTTT app on your phone to send the OTP to the server whenever the OTP SMS is received. Also, you will need a public IP / domain to send the OTP via IFTTT.

When running in this mode, everything is automated. The script will refresh the list of centers in certain intervals and then book if any centers are available. Moreover, if a token becomes invalid, it will send an OTP to the phone number, and the phone will send the OTP to the database using the IFTTT application and then resume with the search.

With the new changes in the Cowin site, there is a maximum of 1000 requests and 50 OTPs per day for a user. After that, the phone number will be blocked for 24 hours (or more). So, running in this mode blindly will undoubtedly hit the limit.

### Start the server
Run the server in a separate window to receive the OTP from the mobile,
```sh
python src/app.py
```
The default port is 5000, and change the debug mode if you want (It is advisable to put up a proxy server in front of the flask server).
### Install IFTTT
Install and set up IFTTT app on the phone. But if it takes about 10-50 seconds for IFTTT to detect and send the OTP. 

- Install the IFTTT app from [playstore](https://play.google.com/store/apps/details?id=com.ifttt.ifttt&hl=en_US&gl=US) / [appstore](https://apps.apple.com/us/app/ifttt/id660944635).
- Choose a new applet.
- In the "If" section, choose android SMS and then "New SMS received matches" and then type "CoWIN" inside the text box.
- In the then section, choose "Webhooks", in the following URL box, fill your server name followed by `/your-phone-number/otp` * e.g.: https://mydomain.com/9999999999/otp*
- The method is `PUT,` the content type is `application/json`, and the body is `{"message": "Text"}` The `Test` in the body is an ingredient inserted by clicking the *Insert Ingredient* button.
- Then continue with the setup.

![IFTTT setup](screenshots/ifttt.png)
**Set up an IFTTT applet to send the OTP to the server after receiving**

### Run the script

Run the script in automatic mode. (The server and IFTTT should be running)

```sh
python src/run.py --auto
```

## Usage

```
usage: run.py [-h] [-a] [-c]

Book Covid-19 vaccine from the cowin.gov.in portal for India

optional arguments:
  -h, --help         show this help message and exit
  -a, --auto         Switch to automatic entry of OTP and refresh interval
  -c, --from-config  Run using the config.toml file without any prompt
```

When running for the first time, you will have to enter the details of the beneficiaries and the vaccine and price preferences. After the first selection of preferences, a `config.toml` file is created in the project folder containing the user's preferences.

You may run the script directly from the configuration using 
```sh
python src/run.py --from-config
```